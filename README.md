## Hi, welcome to my GitLab profile 👋

- 🔭 I’m currently working on private projects on my University
- 🌱 I’m currently learning OOP with java
- 🌎 I’m trying to ***CHANGE THE WORLD***
- 🤔 I’m looking for help with JavaScript
- ⚡ Fun fact: I won money playing video games

 <a href="https://github.com/IgorMariano25/github-readme-stats"><img align="center" src="https://github-readme-stats.vercel.app/api?username=IgorMariano25&show_icons=true&include_all_commits=true&count_private=true&theme=github_dark&hide_border=true" alt="Igor Mariano's github stats" /></a>  <a href="https://github.com/IgorMariano25/github-readme-stats"><img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=IgorMariano25&layout=compact&theme=github_dark&hide_border=true" /></a>


[![GitHub Streak](https://streak-stats.demolab.com?user=IgorMariano25&theme=github-dark-blue&border_radius=7.5&hide_border=true)](https://git.io/streak-stats)

## Contact me 📧
<div align="center">
  <a href="https://instagram.com/igor.mariano_" target="_blank"><img height="40" width="200" src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white"></a>
  <a href = "mailto:igor-mariano@outlook.com" target="_blank"><img height="40" width="230" src="https://img.shields.io/badge/Microsoft_Outlook-0078D4?style=for-the-badge&logo=microsoft-outlook&logoColor=white"></a>
  <a href="https://www.linkedin.com/in/igormarianodev/" target="_blank"><img height="40" width="200" src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>
  <a href="https://gitlab.com/IgorMariano25" target="_blank"><img height="40" width="200" src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white"></a>
 </div>

## Technologies I use in my programming rotine 🖥️
<div align="center">
  <img alt="Jav-icon" height="50" width="100" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/java/java-original.svg" />
  <img alt="React-icon" height="50" width="100" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original.svg">
  <img alt="Js-icon" height="50" width="100" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
  <img alt="HTML-icon" height="50" width="100" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img alt="CSS-icon" height="50" width="100" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
  <img alt="Python-icon" height="50" width="100" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
</div>

## Code Activity 
#### ***Total time coded since Oct 3 2022***
<p><a href="https://wakatime.com/@0b71d094-f479-4007-b768-49233a5edaf4"><img height="30" width="280"src="https://wakatime.com/badge/user/0b71d094-f479-4007-b768-49233a5edaf4.svg" alt="Total time coded since Oct 3 2022"</img></a></p>
<div align="center">
      <img  height="600" width="1000" src="https://wakatime.com/share/@0b71d094-f479-4007-b768-49233a5edaf4/46d7c5b6-4a3b-42f2-bc28-1a54a8ca24e1.svg"></img>
</div>

  <!-- <!-<details>
    <summary>
      <img align="center" height="500" width="800" src="https://wakatime.com/share/@0b71d094-f479-4007-b768-49233a5edaf4/46d7c5b6-4a3b-42f2-bc28-1a54a8ca24e1.svg"></img>
    </summary>
  </details> -->
 </div>
